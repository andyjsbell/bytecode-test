(5) explain some of the ways hashing functions enable blockchain technology

* They cryptographically link the blocks of the chain by hashing the previous block's header. They are also used to build 
a merkle root of the transactions that are included in the block providing efficient means to verify whether a 
transaction is within the block.  They are also used in POW mining to solve a cryptographic puzzle to win the block reward.

(6) briefly explain Bitcoin's UTXO model of transaction validation (separate from POW)

* Each transaction contains multiple inputs and outputs, in which the total inputs must equal the total outputs, which 
therefore gives us a record of all unspent transactions.  A user's wallet would be the sum of all these unspent transaction
outputs or UTXOs.  In a newly created transaction the owner would spend, or create inputs of one or more of the outputs 
and would receive back "change" which would become a new unspent output on that transaction.  In this model we can easily
verify the balance of a wallet, preventing double spend for example, by ensuring that inputs equal outputs and we can
easily validate the balance of any wallet by tracking all transactions for an address and calculating the balance.

(7) what is the structure of a Block in bitcoin and how does it relate to the 'blockchain' (merkle tree vs merkle list of merkle trees)

* We have a block header which among other things contains the hash of the previous block, a nonce and the merkle root.  
The merkle root is the root hash or a merkle tree which is built up from all the transactions within the block.  The block
then contains the set of transactions that make up the block.

(8) what problem/s are POW/POS trying to solve? discuss/compare (byzantine fault tolerance, reaching a single consensus on a p2p network)

* They are both trying to solve the problem of consensus in a decentralized network by helping it to achieve that all nodes within that 
network all agree on the same thing.  POW requires a computationally intensive puzzle to be solved which they are incentivised
with a reward.  They then broadcast this block to the network which can then be validated by the rest of the nodes to be
included in the chain and ensuring that the network is in consensus on what is included, in terms of transactions in that block.
With POS there is no puzzle but rather the block producer is deterministically chosen based on the amount staked by the validator.
One major difference between the two is the energy consumption of POW.  POW also discourages forking where POS doesn't
discourage forking as the validator would receive the same stake on the new fork where in POW they would need to half their 
computing power to mine on both forks.  