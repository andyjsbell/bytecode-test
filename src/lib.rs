// (1) You are a TA at a university, and you want to evaluate your student’s homework
// without executing their (untrusted) code. You decide to write a small
// web-service that takes bytecode as input, and interprets the results.
//
// The bytecode language you need to support includes basic arithmetic and
// variables. The bytecode language is stack, rather than register based.
// ByteCode (right) is given for the following pseudo code (left):
//
//
// function f() {
//
// x = 1                   LOAD_VAL 1
// WRITE_VAR ‘x’
//
// y = 2                   LOAD_VAL 2
// WRITE_VAR ‘y’
//
// return (x + 1) * y      READ_VAR ‘x’
// LOAD_VAL 1
// ADD
//
// READ_VAR ‘y’
// MULTIPLY
//
// RETURN_VALUE
// }
//
//
// Add a data type `ByteCode` that can represent bytecode like in the example
// above, along with an interpreter for said bytecode. Make sure your bytecode
// is flat, i.e. not nested.
//
// (2) Extend your interpreter with loops. In particular:
// (a) Extend your `ByteCode` data type with suitable instructions to support loops
// (b) Modify your interpreter to support said instructions
// (c) Try it out and see if it works :)


#![allow(non_camel_case_types)]

use std::fs::File;
use std::hash::Hash;
use std::io::{BufRead, BufReader};
use std::ops::{Add, AddAssign, Mul};
use std::path::PathBuf;
use std::{collections::HashMap, fs, io, marker::PhantomData};

pub type Stack<T> = Vec<T>;
pub type Variables<N, T> = HashMap<N, T>;

#[derive(Debug)]
pub enum Operation<N, T> {
    LOAD_VAL(T),
    WRITE_VAR(N),
    READ_VAR(N),
    ADD,
    MULTIPLY,
    RETURN_VALUE,
    START_LOOP(T),
    END_LOOP,
}

#[derive(Default)]
pub struct Iteration<T> {
    /// The current iteration
    pub count: T,
    /// The number of iterations
    pub iterations: T,
}

#[derive(Default)]
pub struct ByteCode<N, T> {
    /// Operations to be performed
    pub operations: Vec<Operation<N, T>>,
    /// A pointer to the current operation to be processed
    pub operation_pointer: usize,
    /// An optional pointer, or marker, for a loop or other jump call
    pub marker_pointer: Option<usize>,
    /// The current iteration of a loop
    pub iteration: Iteration<T>,
}

#[derive(Debug)]
pub enum MachineError {
    /// An invalid operand
    InvalidOp,
    /// An unknown variable has been requested
    UnknownVariable,
    /// The stack is empty
    EmptyStack,
    /// An invalid marker has been used for a loop
    InvalidMarker,
    /// No `RETURN_VALUE` has been included in the byte code
    NoReturn,
}

/// A trait describing what our VM can do
pub trait Machine<N, T> {
    /// Load a value onto the stack
    fn load_value(stack: &mut Stack<T>, v: &T);
    /// Pop the stack and store this to a variable name
    fn write_variable(
        stack: &mut Stack<T>,
        vars: &mut Variables<N, T>,
        v: &N,
    ) -> Result<(), MachineError>;
    /// Push a variable onto the stack
    fn read_variable(
        stack: &mut Stack<T>,
        vars: &mut Variables<N, T>,
        v: &N,
    ) -> Result<(), MachineError>;
    /// Add operation for the last two items on the stack
    fn add(stack: &mut Stack<T>) -> Result<(), MachineError>;
    /// Multiply operation for the last two items on the stack
    fn multiply(stack: &mut Stack<T>) -> Result<(), MachineError>;
}

/// An interpreter for out byte code instruction set
pub struct Interpreter<N, T, M: Machine<N, T>>(PhantomData<N>, PhantomData<T>, PhantomData<M>);

impl<N, T, M: Machine<N, T>> Interpreter<N, T, M>
where
    T: Clone + AddAssign + Ord + From<u8>,
{
    /// The byte code is processed in order as operations.  A `RETURN_VALUE` will force a return from
    /// this function with the top of the stack else it will return an error
    pub fn run(mut byte_code: ByteCode<N, T>) -> Result<T, MachineError> {
        let mut stack = vec![];
        let mut vars: Variables<N, T> = Default::default();
        while byte_code.operation_pointer < byte_code.operations.len() {
            let operation = &byte_code.operations[byte_code.operation_pointer];
            match operation {
                Operation::LOAD_VAL(v) => M::load_value(&mut stack, v),
                Operation::WRITE_VAR(v) => M::write_variable(&mut stack, &mut vars, v)?,
                Operation::READ_VAR(v) => M::read_variable(&mut stack, &mut vars, v)?,
                Operation::ADD => M::add(&mut stack)?,
                Operation::MULTIPLY => M::multiply(&mut stack)?,
                Operation::RETURN_VALUE => {
                    return stack.pop().ok_or(MachineError::EmptyStack);
                }
                Operation::START_LOOP(v) => {
                    byte_code.marker_pointer = Some(byte_code.operation_pointer);
                    byte_code.iteration = Iteration::<T> {
                        count: 0.into(),
                        iterations: v.clone(),
                    };
                }
                Operation::END_LOOP => {
                    byte_code.iteration.count += 1.into();
                    if byte_code.iteration.count == byte_code.iteration.iterations {
                        byte_code.marker_pointer = None;
                    } else {
                        byte_code.operation_pointer = byte_code
                            .marker_pointer
                            .ok_or(MachineError::InvalidMarker)?;
                    }
                }
            }
            byte_code.operation_pointer += 1;
        }
        Err(MachineError::NoReturn)
    }
}

/// A simple virtual machine to run our bytecode
struct SimpleVM<N, T>(PhantomData<N>, PhantomData<T>);

impl<N, T> Machine<N, T> for SimpleVM<N, T>
where
    T: Clone + Add<Output = T> + Mul<Output = T>,
    N: Clone + Eq + Hash,
{
    fn load_value(stack: &mut Stack<T>, v: &T) {
        stack.push(v.clone());
    }

    fn write_variable(
        stack: &mut Stack<T>,
        vars: &mut Variables<N, T>,
        v: &N,
    ) -> Result<(), MachineError> {
        // Pop off from stack and store to name
        vars.insert(v.clone(), stack.pop().ok_or(MachineError::InvalidOp)?);

        Ok(())
    }

    fn read_variable(
        stack: &mut Stack<T>,
        vars: &mut Variables<N, T>,
        v: &N,
    ) -> Result<(), MachineError> {
        // Read variable and push onto stack
        stack.push(vars.get(v).ok_or(MachineError::UnknownVariable)?.clone());

        Ok(())
    }

    fn add(stack: &mut Stack<T>) -> Result<(), MachineError> {
        // Pop two items from the stack and push the answer to it
        let op_1 = stack.pop().ok_or(MachineError::InvalidOp)?;
        let op_2 = stack.pop().ok_or(MachineError::InvalidOp)?;
        stack.push(op_1 + op_2);

        Ok(())
    }

    fn multiply(stack: &mut Stack<T>) -> Result<(), MachineError> {
        // Pop two items from the stack and push the answer to it
        let op_1 = stack.pop().ok_or(MachineError::InvalidOp)?;
        let op_2 = stack.pop().ok_or(MachineError::InvalidOp)?;

        stack.push(op_1 * op_2);

        Ok(())
    }
}

#[test]
fn test_initial_byte_code() {
    let byte_code = ByteCode {
        operations: vec![
            Operation::LOAD_VAL(1),
            Operation::WRITE_VAR('x'), // x = 1
            Operation::LOAD_VAL(2),
            Operation::WRITE_VAR('y'), // y = 2
            Operation::READ_VAR('x'),
            Operation::LOAD_VAL(1),
            Operation::ADD,
            Operation::READ_VAR('y'),
            Operation::MULTIPLY,
            Operation::RETURN_VALUE, // return y(4)
        ],
        ..Default::default()
    };

    assert_eq!(
        Interpreter::<_, _, SimpleVM<_, _>>::run(byte_code).expect("byte code runs correctly"),
        4
    );
}

#[test]
fn test_simple_loop() {
    let byte_code = ByteCode {
        operations: vec![
            Operation::LOAD_VAL(1),
            Operation::WRITE_VAR('x'), // x = 1
            Operation::START_LOOP(41), // for i in 0..41
            Operation::READ_VAR('x'),
            Operation::LOAD_VAL(1),
            Operation::ADD,
            Operation::WRITE_VAR('x'), // x += 1
            Operation::END_LOOP,
            Operation::READ_VAR('x'),
            Operation::RETURN_VALUE, // return x(42)
        ],
        ..Default::default()
    };

    assert_eq!(
        Interpreter::<_, _, SimpleVM<_, _>>::run(byte_code).expect("byte code runs correctly"),
        42
    );
}

// (3) Suppose we added the following bytecode instructions to our language:
// SEND_CHANNEL:
// Pops the channel and a value from the stack and send the
// value on the channel using a blocking send
//
// RECV_CHANNEL:
// Pops the channel from the stack, receives a value from the channel
// (this may block), and push the resulting value back onto the stack
//
// SPAWN:
// Pop two functions from the stack and spawn them as concurrent tasks
//
// Describe in a few sentences how each bytecode instruction could be interpreted,
// and how your interpreter or language runtime could deal with the blocking nature
// of the send and the receive instructions.
//

// ** Answer for (3) **
// A channel would be identified by the first value popped from the stack and used to create a unique data struct accessible
// to both sender and receiver threads which would use a conditional variable to synchronize the two running threads such as
// `CondVar`. This would trigger the condition when we have either a value pushed, or sent, or that we have pulled, or received,
// the value.  On the receiving end the value would be pushed onto the stack of the receiving thread.
// For SPAWN I would use an async construct in which both popped functions would be run concurrently.

// (4) Write a function that given a directory, recursively finds all files with a given file
//  extension in that directory and all sub-directories, and counts the number of lines
//  in the file and prints it to stdout.

/// Count lines of file
pub fn count_lines(path: &PathBuf) -> Result<usize, std::io::Error> {
    return if path.is_file() {
        Ok(BufReader::new(File::open(path)?).lines().count())
    } else {
        Ok(0)
    };
}
/// Recursively filter a directory for files with an extension and return the total line count of
/// matched files
pub fn count_lines_in_directory_by_extension(
    path: &PathBuf,
    extension: &str,
) -> Result<usize, io::Error> {
    let paths = fs::read_dir(path)?;
    let mut total_count = 0;
    for path in paths {
        if let Ok(entry) = path {
            if entry.path().is_file() {
                if let Some(ext) = entry.path().extension() {
                    if ext == extension {
                        let count = count_lines(&entry.path()).expect("failed to count lines");
                        println!("{:?} has {} lines", entry.path(), count);
                        total_count += count;
                    }
                }
            } else {
                total_count += count_lines_in_directory_by_extension(&entry.path(), extension)?;
            }
        }
    }

    Ok(total_count)
}

#[test]
fn test_summing_all_lines_in_current_directory() {
    let test_path = &fs::canonicalize("./count_test").expect("valid path");
    let expected_number_of_lines = 8;
    assert_eq!(
        expected_number_of_lines,
        count_lines_in_directory_by_extension(test_path, "txt").unwrap()
    );
}
